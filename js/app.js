
let url = 'https://api.themoviedb.org';
let table = document.getElementById('tb');



fetch(`${url}/3/search/movie?include_adult=false&page=1&language=en-US&query=a&api_key=08ca1d300293084d7d4dab3d4cdb374d`)
.then((resp) => resp.json())
.then(function(data) {
    console.log(data);
    createHtml(data.results);

    toSort();

}).catch(function(error) {
    console.log(error);
    table.innerHTML = 'Введите название фильма в поисковой строке';
});

function showResult(str) {

    let searchQuery = str ? `&query=${str}` : '' ;

    fetch(`${url}/3/search/movie?include_adult=false&page=1&language=en-US${searchQuery}&api_key=08ca1d300293084d7d4dab3d4cdb374d`)
        .then((resp) => resp.json())
        .then(function(data) {
            console.log(data);
            createHtml(data.results);
                toSort();
        })
        .catch(function(error) {
            console.log(error);
            table.innerHTML = 'Введите название фильма в поисковой строке';
        });
}




function createHtml(responseData) {
    let str = '';

    for(let i = 0; i < responseData.length; i++){
        str += `<tr class='row'>
                  <td data-event="ids">${responseData[i].id}</td>
                  <td data-event="title">${responseData[i].title}</td>
                  <td data-event="lang">${responseData[i].original_language}</td>
                  <td data-event="pop">${responseData[i].popularity}</td>
                  <td data-event="votes">${responseData[i].vote_count}</td>
                  <td data-event="rating">${responseData[i].vote_average}</td>
                  <td data-event="date">${responseData[i].release_date}</td>
                </tr>`;
    }

    table.innerHTML = str;


}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function toChangeDOM(self, target) {

    let arrDom = table.querySelectorAll('.row');

    function isSort(sign){
        let operators = {
            '<'(a,b){
                return a < b;
            },
            '>'(a,b){
                return a > b;
            }
        };

        let arr = [].slice.call(arrDom).sort(function(a, b){
            let itemA;
            let itemB;
            if(isNumeric(a.querySelector(`[data-event="${target}"]`).innerText)){
                itemA = +(a.querySelector(`[data-event="${target}"]`).innerText);
                itemB = +(b.querySelector(`[data-event="${target}"]`).innerText);
            }else{
                itemA = a.querySelector(`[data-event="${target}"]`).innerText;
                itemB = b.querySelector(`[data-event="${target}"]`).innerText;
            }

            return operators[sign](itemA, itemB) ? 1 : -1;
        });

        let str = '';
        arr.forEach(function(item, i) {
            str += arr[i].outerHTML;
        });

        table.innerHTML = str;
    }

    if(self.classList.contains('class')){
        isSort('<');
        self.classList.remove("class");
    }else{
        isSort('>');
        self.classList.add("class");
    }
}

function toSort(){
    let table = document.getElementById("table");
    let title = table.querySelectorAll("th");

    title.forEach(function(item, i){
        item.addEventListener('click',function(){
            if(item.hasAttribute('data-target')){
                toChangeDOM(item, item.getAttribute('data-target'));
            }
        });
    });

}






